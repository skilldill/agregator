import { connect } from "react-redux";
import { NewsList } from "../components/NewsList";
import { newsList as newsListActions } from "../store/actions";

const mapStateToProps = (state: any) => ({
    news: state.newsList.news,
    isLoading: state.newsList.isLoading,
});
const mapDispatchToProps = (dispatch: any) => ({
    fetchNews: (query: string) => dispatch(newsListActions.fetchNews(query)),
});

export const NewsListContainer = connect(mapStateToProps, mapDispatchToProps)(NewsList);

import { connect } from "react-redux";

import { SplashLoading } from "../components/SplashLoading";
import { splashLoading as splashLoadingActions } from "../store/actions";

const mapDispatchToProps = (dispatch: any) => ({
    checkSession: (query: string) => dispatch(splashLoadingActions.checkSession(query)),
});

export const SplashLoadingContainer = connect(null, mapDispatchToProps)(SplashLoading);

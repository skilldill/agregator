import { compose } from "redux";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

import { FORMS_NAMES } from "../components/forms/constants";
import { SigninForm } from "../components/forms/SigninForm";
import { signinForm as signinFormActions } from "../store/actions";

const mapStateToProps = (state: any) => ({
    passwordAttention: state.signinForm.passwordAttention,
    attentionForm: state.signinForm.attentionForm
})

const mapDispatchToProps = (dispatch: any) => ({
    onSubmit: (data: any) => dispatch(signinFormActions.submitData(data)),
});

const connectSigninForm = connect(mapStateToProps, mapDispatchToProps);
const reduxSigninForm = reduxForm({ form: FORMS_NAMES.SIGNIN_FORM });

export const SigninFormContainer = compose(connectSigninForm, reduxSigninForm)(SigninForm) as React.ComponentType;

import { compose } from "redux";
import { connect } from "react-redux";
import { reduxForm } from "redux-form";

import { FORMS_NAMES } from "../components/forms/constants";
import { LoginForm } from "../components/forms/LoginForm";
import { loginForm as loginFormActions } from "../store/actions";

const mapStateToProps = (state: any) => ({
    attentionForm: state.loginForm.attentionForm
})

const mapDispatchToProps = (dispatch: any) => ({
    onSubmit: (data: any) => dispatch(loginFormActions.submitData(data)),
});

const connectLoginForm = connect(mapStateToProps, mapDispatchToProps);
const reduxLoginForm = reduxForm({ form: FORMS_NAMES.LOGIN_FORM });

export const LoginFormContainer = compose(connectLoginForm, reduxLoginForm)(LoginForm) as React.ComponentType;

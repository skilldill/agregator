import React from "react";
import { Provider } from "react-redux";
import { HashRouter as Router } from "react-router-dom";

import "./styles/style.scss";
import { store } from "./store";
import { Routes } from "./routes";

const App: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <Routes />
            </Router>
        </Provider>
    );
};

export default App;

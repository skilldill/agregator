import { Dispatch } from "redux";
import { ActionFunctionAny, createAction } from "redux-actions";
import axios from "axios";

import { URLS } from "../../../constants";
// import {newsList as newsListActions} from "../newsList";

class SplashLoading {
    readonly prefix: string = "SPLASH_LOADING"

    checkSession = (query: string) => async (dispatch: Dispatch) => {
        try{
            const response = await axios.get(URLS.checkSession, {withCredentials: true});
            const { is_auth } = response.data; // Тотал пока что не нужен

            console.log(is_auth)

            if(is_auth)
                return window.location.href = `${URLS.domen}/#/user_page`

            return window.location.href = `${URLS.domen}/#/authorization/login`

        } catch(e) {
            // Если что тут опсано, так как редиректить будет при любой ошибке
            window.location.href = `${URLS.domen}/#/authorization/start`
        }
    };
}

export const splashLoading = new SplashLoading()
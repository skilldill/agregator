import { Dispatch } from "redux";
import { ActionFunctionAny, createAction } from "redux-actions";
import axios from "axios";

import { URLS } from "../../../constants";

class SigninForm {
    readonly prefix: string = "SIGNIN_FORM";
    readonly SET_PASSWORD_ATTENTION: string = `${this.prefix}.SET_PASSWORD_ATTENTION`;
    readonly SET_FORM_ATTENTION: string = `${this.prefix}.SET_FORM_ATTENTION`;

    setPasswordAttention: ActionFunctionAny<any> = createAction(this.SET_PASSWORD_ATTENTION); 
    setFormAttention: ActionFunctionAny<any> = createAction(this.SET_FORM_ATTENTION);

    submitData = (data: any) => async (dispatch: Dispatch) => {
        const { login, password, password_check } = data;

        if (password === password_check) {
            try {
                const siginForm = new FormData();
                siginForm.append("login", login);
                siginForm.append("password", password);

                const response = await axios.post(URLS.signin, siginForm);
                
                if(response.data.status === "ok")
                    window.location.href = "/#/authorization/login"

                
            } catch(e) {
    
            }
        } else {
            dispatch(this.setPasswordAttention("Пароли не совпадают"))
        }
    }
}

export const signinForm = new SigninForm();
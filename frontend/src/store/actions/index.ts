import { splashLoading } from "./splashLoading";
import { loginForm } from "./loginForm";
import { signinForm } from "./signinForm";
import { newsList } from "./newsList";

export { splashLoading, loginForm, signinForm, newsList };

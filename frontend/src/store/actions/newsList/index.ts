import { createAction } from "redux-actions";
import axios from "axios";

import { URLS } from "../../../constants";

class NewsList {
    readonly prefix: string = "NEWS_LIST";
    readonly SET_NEWS_LIST: string = `${this.prefix}.SET_NEWS_LIST`;
    readonly SET_LOADING_STATUS: string = `${this.prefix}.SET_LOADING_STATUS`;

    setNewsList = createAction(this.SET_NEWS_LIST);
    setLoadingStatus = createAction(this.SET_LOADING_STATUS);

    fetchNews = (query: string) => async (dispatch: any) => {
        dispatch(this.setLoadingStatus(true));

        try{
            const response = await axios.get(URLS.news, { params: { query }, withCredentials: true });
            const { news } = response.data; // Тотал пока что не нужен
            dispatch(this.setNewsList(news));

        } catch(e) {
            dispatch(this.setLoadingStatus(false));

            // Если что тут опсано, так как редиректить будет при любой ошибке
            window.location.href = `${URLS.domen}/#/authorization/start`
        }
    }
}

export const newsList = new NewsList();
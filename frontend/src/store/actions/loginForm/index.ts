import axios from "axios";
import { createAction } from "redux-actions";

import { URLS } from "../../../constants";
import {newsList as newsListActions} from "../newsList";

class LoginForm {
    readonly prefix: string = "LOGIN_FORM";
    readonly SET_FORM_ATTENTION: string = `${this.prefix}.SET_FORM_ATTENTION`;

    setFormAttention = createAction(this.SET_FORM_ATTENTION);

    submitData = (data: any) => async (dispatch: any) => {
        const loginForm = new FormData();
        loginForm.append("login", data.login);
        loginForm.append("password", data.password);

        try {
            data = JSON.stringify(data)
            const response = await axios.post(URLS.login, loginForm, {withCredentials: true});
            
            // Если залогинились то получаем список новостей
            if(response.data.status === "ok") {
                dispatch(newsListActions.fetchNews("Тюмень"))
                window.location.href = `${URLS.domen}/#/user_page`
            } else {
                dispatch(this.setFormAttention("Неверный логин или пароль"))
            }

        } catch(e) {

        }
    }
}

export const loginForm = new LoginForm();
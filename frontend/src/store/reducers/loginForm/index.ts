import { handleActions, Action } from "redux-actions";

import { loginForm as loginFormActions } from "../../actions";

const initialState = {
    attentionForm: null,
};

const reducerMap = {
    [loginFormActions.SET_FORM_ATTENTION]: (state: any, action: Action<any>) => ({
        ...state,
        attentionForm: action.payload,
    }),
};

export const loginFormReducer = handleActions(reducerMap, initialState);

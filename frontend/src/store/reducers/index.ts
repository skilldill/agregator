import { combineReducers } from "redux";
import { reducer as form } from "redux-form";
import { signinFormReducer as signinForm } from "./signinForm";
import { loginFormReducer as loginForm } from "./loginForm";
import { newsListReducer as newsList } from "./newsList";

export const reducers = combineReducers({
    form,
    signinForm,
    loginForm,
    newsList,
});

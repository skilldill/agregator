import { Action, handleActions } from "redux-actions";

import { signinForm as signinFormActions } from "../../actions";

const initialState = {
    passwordAttention: null,
    attentionForm: null,
};

const reducerMap = {
    [signinFormActions.SET_PASSWORD_ATTENTION]: (state: any, action: Action<any>) => ({
        ...state,
        passwordAttention: action.payload,
    }),
    [signinFormActions.SET_FORM_ATTENTION]: (state: any, action: Action<any>) => ({
        ...state,
        attentionForm: action.payload,
    }),
};

export const signinFormReducer = handleActions(reducerMap, initialState);

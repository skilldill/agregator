import { handleActions, Action } from "redux-actions";

import { newsList as newsListActions } from "../../actions";

const initialState = {
    news: null,
    isLoading: false,
};

const reducerMap = {
    [newsListActions.SET_NEWS_LIST]: (state: any, action: Action<any>) => ({
        ...state,
        news: [...action.payload],
        isLoading: false,
    }),
    [newsListActions.SET_LOADING_STATUS]: (state: any, action: Action<any>) => ({
        ...state,
        isLoading: action.payload,
    }),
};

export const newsListReducer = handleActions(reducerMap, initialState);

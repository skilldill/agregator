import React, { useEffect, useState } from "react";
import axios from "axios";

import "./style.scss";
import { URLS } from "../../constants";
import { SideMenu } from "../SideMenu";
import { SearchBlock } from "../SearchBlock";

const NewsCard = (props: any) => {
    const [marked, setMarked] = useState(false);

    return (
        <div className="news_card">
            <div className={`news_card__header ${!props.owner && "news_card__header-end"}`}>
                {props.owner && <h3 className="news_card__owner">{props.owner}</h3>}
                <span className="news_card__date">
                    <ClockIcon />
                    <small>{props.date}</small>
                </span>
            </div>
            <h2 className="news_card__title">{props.title}</h2>
            {props.body && <p className="news_card__text">{props.body}</p>}
            {props.content && <p className="news_card__text">{props.content}</p>}
            <span className="news_card__read_next">Читать продолжение...</span>
            {props.img && <img src={props.img} style={{ width: "100%" }} />}
            {props.watter_mark && <span>{props.watter_mark}</span>}
            <div className="controls">
                <span
                    onClick={() => {
                        let winNavigarot: any = window.navigator;

                        if (winNavigarot && winNavigarot.share) {
                            winNavigarot
                                .share({
                                    title: "Поделиться новостью",
                                    url: props.ref,
                                })
                                .then(() => {
                                    console.log("Good");
                                })
                                .catch(console.error);
                        } else {
                        }
                    }}
                >
                    <Share />
                </span>
                <span
                    onClick={() => {
                        setMarked(!marked);
                    }}
                >
                    {marked ? <MarkSaveFill /> : <MarkSave />}
                </span>
            </div>
        </div>
    );
};

const Burger = () => (
    <svg width="18" height="16" viewBox="0 0 18 16" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M0 15.875V13.625H9V15.875H0ZM0 9.125V6.875H18V9.125H0ZM0 2.375V0.125H18V2.375H0Z"
            fill="#FF7E2E"
        />
    </svg>
);

const ClockIcon = () => (
    <svg width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M2.24973 2.24973C2.90966 1.51686 3.72092 0.936104 4.62739 0.547617C5.53386 0.159131 6.5139 -0.0278102 7.49972 -0.000274956C8.48555 -0.0278102 9.46559 0.159131 10.3721 0.547617C11.2785 0.936104 12.0898 1.51686 12.7497 2.24973C13.4826 2.90966 14.0633 3.72092 14.4518 4.62739C14.8403 5.53386 15.0273 6.5139 14.9997 7.49972C15.0273 8.48555 14.8403 9.46559 14.4518 10.3721C14.0633 11.2785 13.4826 12.0898 12.7497 12.7497C12.0898 13.4826 11.2785 14.0633 10.3721 14.4518C9.46559 14.8403 8.48555 15.0273 7.49972 14.9997C6.5139 15.0273 5.53386 14.8403 4.62739 14.4518C3.72092 14.0633 2.90966 13.4826 2.24973 12.7497C0.835244 11.3659 0.0262743 9.47834 -0.000274956 7.49972C-0.0278102 6.5139 0.159131 5.53386 0.547617 4.62739C0.936104 3.72092 1.51686 2.90966 2.24973 2.24973ZM10.8747 10.8747L11.7494 10L8.62472 6.87441L7.49972 1.87472H6.25004V7.49972C6.24546 7.66398 6.27663 7.82727 6.34138 7.9783C6.40612 8.12932 6.50291 8.26448 6.62504 8.37441C6.69256 8.44228 6.78051 8.48609 6.87535 8.4991L10.8747 10.8747Z"
            fill="#7E2EFF"
        />
    </svg>
);

const Share = () => (
    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
        <mask id="path-2-inside-1" fill="white">
            <path
                fill-rule="evenodd"
                clip-rule="evenodd"
                d="M11.6917 3.55566L0.888916 11.3502L11.6917 18.8537V14.3177C12.7413 14.229 18.6626 14.042 22.5849 20.57H23.2577C23.2577 20.57 24.1336 8.27243 11.6917 8.17431V3.55566Z"
            />
        </mask>
        <path
            d="M0.888916 11.3502L0.303795 10.5392L-0.841648 11.3657L0.318437 12.1715L0.888916 11.3502ZM11.6917 3.55566H12.6917V1.60101L11.1065 2.74472L11.6917 3.55566ZM11.6917 18.8537L11.1212 19.675L12.6917 20.7658V18.8537H11.6917ZM11.6917 14.3177L11.6074 13.3212L10.6917 13.3986V14.3177H11.6917ZM22.5849 20.57L21.7277 21.085L22.0191 21.57H22.5849V20.57ZM23.2577 20.57V21.57H24.189L24.2551 20.641L23.2577 20.57ZM11.6917 8.17431H10.6917V9.16646L11.6838 9.17428L11.6917 8.17431ZM1.47404 12.1611L12.2768 4.36661L11.1065 2.74472L0.303795 10.5392L1.47404 12.1611ZM12.2621 18.0324L1.45939 10.5288L0.318437 12.1715L11.1212 19.675L12.2621 18.0324ZM10.6917 14.3177V18.8537H12.6917V14.3177H10.6917ZM11.7759 15.3141C12.6632 15.2391 18.0935 15.0364 21.7277 21.085L23.4421 20.055C19.2317 13.0475 12.8193 13.2188 11.6074 13.3212L11.7759 15.3141ZM22.5849 21.57H23.2577V19.57H22.5849V21.57ZM23.2577 20.57C24.2551 20.641 24.2552 20.6405 24.2552 20.6399C24.2552 20.6397 24.2553 20.639 24.2553 20.6385C24.2554 20.6375 24.2555 20.6363 24.2556 20.6348C24.2557 20.632 24.256 20.6285 24.2562 20.6241C24.2568 20.6155 24.2575 20.604 24.2583 20.5895C24.2599 20.5607 24.2618 20.5204 24.2637 20.4693C24.2676 20.3673 24.2712 20.2223 24.271 20.0403C24.2706 19.6768 24.2551 19.1638 24.1954 18.5502C24.0766 17.3285 23.7799 15.679 23.0581 14.0117C22.334 12.339 21.1728 10.6287 19.3211 9.33316C17.4643 8.03406 14.9818 7.20023 11.6995 7.17434L11.6838 9.17428C14.6224 9.19745 16.6962 9.93761 18.1745 10.9719C19.6579 12.0097 20.6106 13.3922 21.2227 14.8062C21.8371 16.2255 22.0991 17.6566 22.2048 18.7438C22.2574 19.2846 22.2707 19.733 22.271 20.0424C22.2712 20.1969 22.2681 20.3162 22.2652 20.3944C22.2637 20.4335 22.2623 20.4622 22.2613 20.4799C22.2608 20.4887 22.2604 20.4948 22.2602 20.498C22.2601 20.4996 22.2601 20.5005 22.2601 20.5007C22.2601 20.5007 22.2601 20.5006 22.2601 20.5003C22.2601 20.5002 22.2601 20.4998 22.2601 20.4998C22.2601 20.4994 22.2602 20.4989 23.2577 20.57ZM10.6917 3.55566V8.17431H12.6917V3.55566H10.6917Z"
            fill="#FF7E2E"
            mask="url(#path-2-inside-1)"
        />
    </svg>
);

const MarkSave = () => (
    <svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M6.64645 10.8964L0.75 16.7929V0.5H13.25V16.7929L7.35355 10.8964L7 10.5429L6.64645 10.8964Z"
            stroke="#FF7E2E"
        />
    </svg>
);

const MarkSaveFill = () => (
    <svg width="14" height="18" viewBox="0 0 14 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M0.25 0H13.75V18L7 11.25L0.25 18V0Z" fill="#FF7E2E" />
    </svg>
);

const Zoomer = () => (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M17.8875 16.3125L14.175 12.6C15.2088 11.2429 15.7627 9.58103 15.75 7.87508C15.7564 6.83914 15.5571 5.81223 15.1637 4.85389C14.7702 3.89556 14.1904 3.02488 13.4578 2.29235C12.7253 1.55981 11.8546 0.979989 10.8963 0.586511C9.93794 0.193034 8.91103 -0.00627259 7.87508 0.000150443C6.83914 -0.00627259 5.81223 0.193034 4.85389 0.586511C3.89556 0.979989 3.02488 1.55981 2.29235 2.29235C1.55981 3.02488 0.979989 3.89556 0.586511 4.85389C0.193034 5.81223 -0.00627259 6.83914 0.000150443 7.87508C-0.00627259 8.91103 0.193034 9.93794 0.586511 10.8963C0.979989 11.8546 1.55981 12.7253 2.29235 13.4578C3.02488 14.1904 3.89556 14.7702 4.85389 15.1637C5.81223 15.5571 6.83914 15.7564 7.87508 15.75C9.58103 15.7627 11.2429 15.2088 12.6 14.175L16.3125 17.8875L17.8875 16.3125ZM2.25013 7.87508C2.24276 7.13436 2.38323 6.3996 2.6633 5.71382C2.94337 5.02804 3.35742 4.40502 3.88122 3.88122C4.40502 3.35742 5.02804 2.94337 5.71382 2.6633C6.3996 2.38323 7.13436 2.24276 7.87508 2.25013C8.61581 2.24276 9.35057 2.38323 10.0363 2.6633C10.7221 2.94337 11.3451 3.35742 11.8689 3.88122C12.3927 4.40502 12.8068 5.02804 13.0869 5.71382C13.3669 6.3996 13.5074 7.13436 13.5 7.87508C13.5074 8.61581 13.3669 9.35057 13.0869 10.0363C12.8068 10.7221 12.3927 11.3451 11.8689 11.8689C11.3451 12.3927 10.7221 12.8068 10.0363 13.0869C9.35057 13.3669 8.61581 13.5074 7.87508 13.5C7.13436 13.5074 6.3996 13.3669 5.71382 13.0869C5.02804 12.8068 4.40502 12.3927 3.88122 11.8689C3.35742 11.3451 2.94337 10.7221 2.6633 10.0363C2.38323 9.35057 2.24276 8.61581 2.25013 7.87508Z"
            fill="#FF7E2E"
        />
    </svg>
);

const Navbar = (props: any) => {
    return (
        <div className="navbar">
            <div className="navbar__part">
                <span style={{ marginLeft: "20px" }} onClick={props.openMenu}>
                    <Burger />
                </span>
                <h3 className="navbar__title" style={{ marginLeft: "22px" }}>
                    {props.title}
                </h3>
            </div>
            <div className="navbar__part">
                <MarkSaveFill />
                <span
                    style={{ marginLeft: "22px", marginRight: "20px" }}
                    onClick={props.openSearch}
                >
                    <Zoomer />
                </span>
            </div>
        </div>
    );
};

export const NewsList = (props: any) => {
    const [showMenu, setShowMenu] = useState(false);
    const [showSearchBlock, setShowSearchBlock] = useState(false);
    const [titleBar, setTitleBar] = useState("Главное");

    useEffect(() => {
        props.fetchNews("Главное");
    }, [props.fetchNews]);

    const queries = [
        "Главное",
        "Местные новости",
        "Происшествия",
        "Культура",
        "Наука и техника",
        "Спорт",
    ];
    return (
        <>
            <div className="news_list" style={{ height: !props.news ? "100vh" : "auto" }}>
                <Navbar
                    title={titleBar}
                    openMenu={() => setShowMenu(true)}
                    openSearch={() => setShowSearchBlock(true)}
                />

                {props.news &&
                    !props.isLoading &&
                    props.news.map((item: any, i: number) => <NewsCard key={i} {...item} />)}
                {props.isLoading && <div className="splash">Поиск новостей...</div>}
            </div>
            {showMenu && (
                <SideMenu
                    items={queries}
                    find={(query: string) => {
                        props.fetchNews(query);
                        setTitleBar(query);
                    }}
                    close={() => {
                        setShowMenu(false);
                    }}
                />
            )}
            {showSearchBlock && (
                <SearchBlock
                    close={() => setShowSearchBlock(false)}
                    find={(query: string) => {
                        props.fetchNews(query);
                        setTitleBar(query);
                    }}
                />
            )}
        </>
    );
};

import React from "react";
import { Link } from "react-router-dom";

import "./style.scss";
import authMan from "../../assets/auth_man.png";

export const StartPage = (props: any) => {
    return (
        <div className="start">
            <h1 className="start__title">.NEWS</h1>
            <img src="" alt="" />
            <p className="start__description">
                Приложение, которое создано для новостей понятных для людей
            </p>
            <img className="start__img" src={authMan} alt="man with phone" />
            <Link className="start__btn" to="/authorization/login">
                Вход
            </Link>
            <Link className="start__btn" to="/authorization/signin">
                Регистрация
            </Link>
        </div>
    );
};

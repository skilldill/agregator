import React, { useState } from "react";

import "./style.scss";

const ArrowLeft = () => (
    <svg width="13" height="21" viewBox="0 0 13 21" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            fill-rule="evenodd"
            clip-rule="evenodd"
            d="M9.75946 0.412269L0.395654 9.40223C-0.131885 9.90796 -0.131885 10.7522 0.395654 11.26L9.75946 20.2479C10.3337 20.7993 11.2403 20.7993 11.8146 20.2479C12.4221 19.6643 12.4221 18.6923 11.8146 18.1077L3.97212 10.5788C3.82985 10.4428 3.82985 10.2174 3.97212 10.0814L11.8146 2.55254C12.4221 1.96892 12.4221 0.996923 11.8146 0.412269C11.5269 0.138115 11.1572 0 10.7875 0C10.4168 0 10.0471 0.138115 9.75946 0.412269Z"
            fill="#FF7E2E"
        />
    </svg>
);

const Zoomer = () => (
    <svg width="18" height="18" viewBox="0 0 18 18" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M17.8875 16.3125L14.175 12.6C15.2088 11.2429 15.7627 9.58103 15.75 7.87508C15.7564 6.83914 15.5571 5.81223 15.1637 4.85389C14.7702 3.89556 14.1904 3.02488 13.4578 2.29235C12.7253 1.55981 11.8546 0.979989 10.8963 0.586511C9.93794 0.193034 8.91103 -0.00627259 7.87508 0.000150443C6.83914 -0.00627259 5.81223 0.193034 4.85389 0.586511C3.89556 0.979989 3.02488 1.55981 2.29235 2.29235C1.55981 3.02488 0.979989 3.89556 0.586511 4.85389C0.193034 5.81223 -0.00627259 6.83914 0.000150443 7.87508C-0.00627259 8.91103 0.193034 9.93794 0.586511 10.8963C0.979989 11.8546 1.55981 12.7253 2.29235 13.4578C3.02488 14.1904 3.89556 14.7702 4.85389 15.1637C5.81223 15.5571 6.83914 15.7564 7.87508 15.75C9.58103 15.7627 11.2429 15.2088 12.6 14.175L16.3125 17.8875L17.8875 16.3125ZM2.25013 7.87508C2.24276 7.13436 2.38323 6.3996 2.6633 5.71382C2.94337 5.02804 3.35742 4.40502 3.88122 3.88122C4.40502 3.35742 5.02804 2.94337 5.71382 2.6633C6.3996 2.38323 7.13436 2.24276 7.87508 2.25013C8.61581 2.24276 9.35057 2.38323 10.0363 2.6633C10.7221 2.94337 11.3451 3.35742 11.8689 3.88122C12.3927 4.40502 12.8068 5.02804 13.0869 5.71382C13.3669 6.3996 13.5074 7.13436 13.5 7.87508C13.5074 8.61581 13.3669 9.35057 13.0869 10.0363C12.8068 10.7221 12.3927 11.3451 11.8689 11.8689C11.3451 12.3927 10.7221 12.8068 10.0363 13.0869C9.35057 13.3669 8.61581 13.5074 7.87508 13.5C7.13436 13.5074 6.3996 13.3669 5.71382 13.0869C5.02804 12.8068 4.40502 12.3927 3.88122 11.8689C3.35742 11.3451 2.94337 10.7221 2.6633 10.0363C2.38323 9.35057 2.24276 8.61581 2.25013 7.87508Z"
            fill="#FF7E2E"
        />
    </svg>
);

export const SearchBlock = (props: any) => {
    const [query, setQuery] = useState("");

    const history =
        localStorage.getItem("history") && JSON.parse(localStorage.getItem("history") || "");

    return (
        <div className="search_block">
            <div>
                <div className="search_block__bar">
                    <span onClick={props.close}>
                        <ArrowLeft />
                    </span>
                    <div className="search_block__input">
                        <span
                            onClick={() => {
                                props.find && props.find(query);
                                props.close();

                                if (history && history.length > 0) {
                                    history.push(query);
                                    localStorage.setItem("history", JSON.stringify(history));
                                } else {
                                    localStorage.setItem("history", JSON.stringify([query]));
                                }

                                setQuery("");
                            }}
                        >
                            <Zoomer />
                        </span>
                        <input
                            type="text"
                            placeholder="поиск"
                            onChange={(e) => setQuery(e.currentTarget.value)}
                            value={query}
                        />
                    </div>
                </div>
            </div>
            <div className="search_block__history">
                <h3 style={{ paddingLeft: "20px" }}>История поиска</h3>
                <ul style={{ paddingLeft: "20px" }}>
                    {history &&
                        history.length > 0 &&
                        history.map((item: string, i: number) => (
                            <li
                                style={{ marginTop: "15px" }}
                                key={i}
                                onClick={() => {
                                    props.find && props.find(item);
                                    props.close();
                                    setQuery("");
                                }}
                            >
                                {item}
                            </li>
                        ))}
                </ul>
            </div>
        </div>
    );
};

import React from "react";
import axios from "axios";

import "./style.scss";
import { URLS } from "../../constants";

const TimesIcon = () => (
    <svg width="17" height="17" viewBox="0 0 17 17" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
            d="M8.49945 10.0446L1.54516 17L0 15.4538L6.9543 8.49945L0 1.54516L1.54516 0L8.49945 6.9543L15.4538 0L17 1.54516L10.0446 8.49945L16.9989 15.4538L15.4527 17L8.49945 10.0446Z"
            fill="#FF7E2E"
        />
    </svg>
);

export const SideMenu = (props: any) => (
    <div className="sidemenu">
        <div className="sidemenu__close" onClick={props.close}>
            <TimesIcon />
        </div>
        <h2 className="sidemenu__title">.NEWS</h2>
        <ul className="sidemenu__items">
            {props.items &&
                props.items.map((query: any, i: number) => (
                    <li
                        key={i}
                        className="sidemenu__item"
                        onClick={() => {
                            props.close();
                            props.find && props.find(query);
                        }}
                    >
                        {query}
                    </li>
                ))}
        </ul>
        <div className="controls">
            <button>Найтройки</button>
            <button
                onClick={async () => {
                    await axios.get(URLS.logout, { withCredentials: true });
                    window.location.href = `${URLS.domen}/#/authorization/login`;
                }}
            >
                Выход
            </button>
        </div>
    </div>
);

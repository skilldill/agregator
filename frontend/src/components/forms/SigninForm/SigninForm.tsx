import React from "react";
import { Link } from "react-router-dom";

import "../style.scss";
import { InputControl } from "../InputControl";

export const SigninForm = (props: any) => {
    const { attentionForm, handleSubmit } = props;

    return (
        <form className="form" onSubmit={handleSubmit}>
            <h2 className="form__title">Регистрация</h2>
            <InputControl
                name="login"
                placeholder="Логин"
                component={(fielProps: any) => (
                    <input {...fielProps.input} type="text" placeholder="Логин" />
                )}
            />
            <InputControl name="password" component="input" type="password" placeholder="Пароль" />
            <InputControl
                name="password_check"
                component="input"
                type="password"
                placeholder="Повторите пароль"
            />
            <div className="form__control">
                <input className="form__btn" type="submit" value="Создать аккаунт" />
            </div>
            {attentionForm && alert(attentionForm)}
        </form>
    );
};

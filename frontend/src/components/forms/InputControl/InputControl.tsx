import React from "react";
import { Field } from "redux-form";

import "../style.scss";

export const InputControl = (props: any) => {
    const { name, component, placeholder, type } = props;
    return (
        <div className="form__control">
            <Field name={name} component={component} placeholder={placeholder} type={type} />
        </div>
    );
};

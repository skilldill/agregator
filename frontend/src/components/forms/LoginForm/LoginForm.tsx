import React from "react";
import { Link } from "react-router-dom";

import "../style.scss";
import { InputControl } from "../InputControl";

export const LoginForm = (props: any) => {
    const { attentionForm, handleSubmit } = props;

    return (
        <form className="form" onSubmit={handleSubmit}>
            <h2 className="form__title">Вход</h2>
            <InputControl
                name="login"
                component={(fielProps: any) => (
                    <input {...fielProps.input} type="text" placeholder="Логин" />
                )}
            />
            <InputControl name="password" component="input" type="password" placeholder="Пароль" />
            <div className="form__control">
                <input className="form__btn" type="submit" value="Войти" />
            </div>
            {attentionForm && alert(attentionForm)}
        </form>
    );
};

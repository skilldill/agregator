import React, { useEffect } from "react";

import "./style.scss";

export const SplashLoading = (props: any) => {
    useEffect(() => {
        props.checkSession();
    }, [props.checkSession]);

    return <div className="splash">Загрузка...</div>;
};

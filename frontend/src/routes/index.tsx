import React from "react";
import { Switch } from "react-router";
import { Route } from "react-router-dom";

import { URLS } from "../constants";
import { Authorization } from "./Authorization";
import { SplashLoadingContainer, NewsListContainer } from "../containers";

export const Routes = () => (
    <Switch>
        <Route exact path={`${URLS.domen}/`}>
            <SplashLoadingContainer />
        </Route>
        <Route path={`${URLS.domen}/authorization`}>
            <Authorization />
        </Route>
        <Route path={`${URLS.domen}/user_page`}>
            <NewsListContainer />
        </Route>
    </Switch>
);

export const URLS = {
    domen: process.env.REACT_APP_DOMEN_URL || "",
    news: `${process.env.REACT_APP_BASE_API_URL}/news`,
    login: `${process.env.REACT_APP_BASE_API_URL}/login`,
    signin: `${process.env.REACT_APP_BASE_API_URL}/signin`,
    logout: `${process.env.REACT_APP_BASE_API_URL}/logout`,
    checkSession: `${process.env.REACT_APP_BASE_API_URL}/check`,
};

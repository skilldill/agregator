from flask import Flask
from flask_restful import Api
from flask_cors import CORS
import json

from resources import *

app = Flask(__name__)
CORS(app, resource={r"/*": {"origins": "*"}})

with open('config.json', 'r') as f:
    o = json.load(f)
    app.secret_key = o['secret_key']

api = Api(app)
api.prefix = "/api/v1"


api.add_resource(Signin, "/signin", "signin")
api.add_resource(Login, "/login", "login")
api.add_resource(Logout, "/logout", "logout")
api.add_resource(NewsRecource, "/news", "news")
api.add_resource(LoginAdmin, "/admin/api/login", "admin/api/login")
api.add_resource(LogoutAdmin, "/admin/api/logout", "admin/api/logout")
api.add_resource(NewsResourceAdmin, "/admin/api/news", "admin/api/news")

if __name__ == "__main__":
    app.run(debug=True)

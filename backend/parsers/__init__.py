from .RbkParser import RbkParser
from .CosmopolitanParser import CosmopolitanParser
from .RiaParser import RiaParser

rbk_parser = RbkParser()
cosmopolitan_parser = CosmopolitanParser()
ria_parser = RiaParser() 
from bs4 import BeautifulSoup as bs

SELECTORS_MIN_BLOCK = {
    "block": {
        "tag": "",
        "class": "list-item"
    },
    "title": {
        "tag": "",
        "class": "list-item__title color-font-hover-only"
    },
    "body": {
        "class": ""
    },
    "ref": {
        "class": "list-item__image"
    },
    "date": {
        "class": "list-item__date"
    },
    "img": {
        "class": "responsive_img m-list-img"
    }
}

SELECTORS_BLOCK = {
    "block": {
        "class": ""
    },
    "title": {
        "tag": "",
        "class": "article__title"
    },
    "body": {
        "tag": "",
        "class": "article__text"
    }
}


class RiaParser:
    '''
        Использование материалов
        © и словами МИА «Россия сегодня»
    '''

    def __init__(self):
        self.soup = None

    def pars_list_news(self, html):
        '''
            Возвращает массив новостей в формате json
            example: https://ria.ru/search/?query=Тюмень
        '''
        try:
            self.soup = bs(html, features="html.parser")

            self.news_list = self.soup.find_all(
                class_=SELECTORS_MIN_BLOCK["block"]["class"])

            news_list = []

            for news in self.news_list:
                title = news.find(
                    class_=SELECTORS_MIN_BLOCK["title"]["class"]).get_text()
                date = news.find(
                    class_=SELECTORS_MIN_BLOCK["date"]["class"]).get_text()
                ref = news.find(
                    class_=SELECTORS_MIN_BLOCK["ref"]["class"]).get("href")
                img = news.find(
                    class_=SELECTORS_MIN_BLOCK["img"]["class"]).get("src")

                prepared_news = {
                    "title": title,
                    "date": date,
                    "ref": ref,
                    "watter_mark": "© МИА «Россия сегодня»",
                    "img": img
                }

                news_list.append(prepared_news)

            return news_list

        except:
            return []

    def pars_page_view_news(self, html):
        '''Возвращает новость в формате json'''

        self.soup = bs(html, features="html.parser")

        title = self.soup.find(
            class_=SELECTORS_BLOCK["title"]["class"]).get_text()
        paragraphs = self.soup.find_all(
            class_=SELECTORS_BLOCK["body"]["class"])

        body = ""

        for p in paragraphs:
            body += p.get_text()

        news = {
            "title": title,
            "body": body
        }

        return news

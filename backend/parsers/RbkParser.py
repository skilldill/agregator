from bs4 import BeautifulSoup as bs
from .BaseParser import BaseParser


SELECTORS_MIN_BLOCK = {
    "block": {
        "class": "search-item js-search-item"
    },
    "title": {
        "class": "search-item__title"
    },
    "body": {
        "class": "search-item__text"
    },
    "ref": {
        "class": "search-item__link"
    },
    "date": {
        "class": "search-item__category"
    }
}

SELECTORS_BLOCK = {
    "block": {
        "class": ""
    },
    "title": {
        "class": "js-slide-title"
    },
    "body": {
        "class": "article__text article__text_free"
    }
}


class RbkParser(BaseParser):

    def pars_list_news(self, html):
        '''
            Возвращает массив новостей в формате json
            example: https://t.rbc.ru/search/?project=rbcnews&query=Тюмень
        '''
        try:
            self.soup = bs(html, features="html.parser")
            self.news_list = self.soup.find_all(
                class_=SELECTORS_MIN_BLOCK["block"]["class"])

            news_list = []

            for news in self.news_list:
                title = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["title"]["class"]).get_text()),
                body = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["body"]["class"]).get_text()),
                ref = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["ref"]["class"]).get("href")),
                date = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["date"]["class"]).get_text())

                prepared_news = {
                    "title": title[0],
                    "body": body[0],
                    "ref": ref[0],
                    "date": date
                }

                news_list.append(prepared_news)

            return news_list
        except:
            return []

    def pars_page_view_news(self, html):
        '''
            Возвращает новость в формате json
            example: https://t.rbc.ru/tyumen/17/12/2019/5df8cd489a79478decaa04a0
        '''

        self.soup = bs(html, features="html.parser")

        prepared_text = ""

        paragraphs = self.soup.find(class_="").find_all("p")

        for p in paragraphs:
            prepared_text += p.get_text()

        title = "".join(self.soup.find(
            class_=SELECTORS_BLOCK["title"]["class"]).get_text())
        body = prepared_text

        news = {
            "title": title,
            "body": body
        }

        return news

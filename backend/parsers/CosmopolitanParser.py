from bs4 import BeautifulSoup as bs
from .BaseParser import BaseParser

SELECTORS_MIN_BLOCK = {
    "block": {
        "tag": "a",
        "class": "article-listed is-item"
    },
    "title": {
        "tag": "h2",
        "class": ""
    },
    "body": {
        "class": "preview-text"
    },
    "ref": {
        "class": "article-listed is-item"
    },
    "date": {
        "class": "post-date"
    }
}

SELECTORS_BLOCK = {
    "block": {
        "class": "article-body"
    },
    "title": {
        "tag": "h1",
        "class": "headline"
    },
    "body": {
        "tag": "p",
        "class": ""
    }
}


class CosmopolitanParser(BaseParser):
    def __init__(self):
        self.prefix_ref = "https://www.cosmo.ru"

    def pars_list_news(self, html):
        '''
            Возвращает массив новостей в формате json
            example: https://www.cosmo.ru/search/?query=Тюмень
        '''

        try:
            self.soup = bs(html, features="html.parser")
            self.news_list = self.soup.find_all(
                class_=SELECTORS_MIN_BLOCK["block"]["class"])

            news_list = []

            for news in self.news_list:
                title = "".join(
                    news.find(SELECTORS_MIN_BLOCK["title"]["tag"]).get_text()),
                body = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["body"]["class"]).get_text()),
                date = "".join(
                    news.find(class_=SELECTORS_MIN_BLOCK["date"]["class"]).get_text()),
                ref = f"{self.prefix_ref}{news.get('href')}"

                prepared_news = {
                    "title": title[0],
                    "body": body[0],
                    "date": date,
                    "ref": ref
                }

                news_list.append(prepared_news)

            return news_list
        except:
            return []

    def pars_page_view_news(self, html):
        '''
            Возвращает новость в формате json
            example: https://www.cosmo.ru/lifestyle/news/09-01-2019/ulybku-vizazhistki-iz-tyumeni-priznali-samoy-krasivoy-vo-vselennoy/
        '''

        self.soup = bs(html, features="html.parser")

        block = self.soup.find(class_=SELECTORS_BLOCK["block"]["class"])
        title = self.soup.find(SELECTORS_BLOCK["title"]["tag"]).get_text()
        paragraphs = block.find_all(SELECTORS_BLOCK["body"]["tag"])

        body = ""

        for p in paragraphs:
            body += p.get_text()

        prepared_news = {
            "title": title,
            "body": body
        }

        return prepared_news

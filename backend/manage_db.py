import sqlite3
from hashlib import sha1


def get_hash(value: str) -> str:
    hash = sha1(value.encode())
    result_hash = hash.hexdigest()

    return result_hash


QUERY_CREATE_TABLE_USERS = """
    CREATE TABLE users (
        id INTEGER PRIMARY KEY
        , login VARCHAR(200) NOT NULL
    )
"""

QUERY_CREATE_TABLE_PASSWORDS = """
    CREATE TABLE passwords (
        id INTEGER PRIMARY KEY
        , password VARCHAR(200) NOT NULL
        , user_id INTEGER NOT NULL
    )
"""

QUERY_CREATE_TABEL_USERS_DATA = """
    CREATE TABLE users_data (
        id INTEGER PRIMARY KEY
        , email VARCHAR(200)
        , about TEXT
        , avatar BLOB
        , user_id INTEGER NOT NULL
    )
"""


def QUERY_CREATE_TABEL_USER_INTERESTINGS(user_name): return f"""
    CREATE TABLE {user_name}_interestings (
        id INTEGER PRIMARY KEY
        , title VARCHAR(300)
        , content TEXT
        , photo BLOB
    )
"""


QUERY_CREATE_TABLE_NEWSES = """
    CREATE TABLE agregated_news (
        id INTEGER PRIMARY KEY
        , title VARCHAR(300) NOT NULL
        , content TEXT NOT NULL
        , date DATE
        , photo BLOB
    )
"""


conn = sqlite3.connect("data.db")
cursor = conn.cursor()

# cursor.execute(QUERY_CREATE_TABLE_USERS)
# cursor.execute(QUERY_CREATE_TABLE_PASSWORDS)
# cursor.execute(QUERY_CREATE_TABEL_USERS_DATA)
cursor.execute(QUERY_CREATE_TABLE_NEWSES)

cursor.close()
conn.close()

from flask import request
from flask_restful import Resource, abort

from controls import user_control


class Signin(Resource):
    def post(self):
        login = request.form["login"]
        if "admin" in login.lower():
            return {"status": "user already exist"}

        result = user_control.signin(request.form)
        return result

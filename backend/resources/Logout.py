from flask import session
from flask_restful import Resource

class Logout(Resource):
    def get(self):
        try:
            session.pop("user")
            return {"status": "ok"}
        except:
            return {"status": "err"}
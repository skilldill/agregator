from flask import request, session
from flask_restful import Resource

from controls import user_control


class LoginAdmin(Resource):
    def post(self):
        try:
            result = user_control.login(request.form)

            if result["status"] == "ok":
                session["admin"] = request.form["login"]

            return result

        except:
            return {"status": "err"}

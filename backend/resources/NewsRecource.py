from flask import request as flask_request, session
from flask_restful import Resource, abort
from requests import request

from .constants import QUERIES
from parsers import ria_parser, cosmopolitan_parser, rbk_parser
from controls import admin_news_control


class NewsRecource(Resource):
    def get(self):
        if session.get("user"):
            query = flask_request.args['query']

            try:
                html_rbk = request("get", QUERIES["rbk"](query)).text
                html_ria = request("get", QUERIES["ria"](query)).text
                html_cosmo = request("get", QUERIES["cosmo"](query)).text

                rbk_news_list = rbk_parser.pars_list_news(html_rbk)
                ria_news_list = ria_parser.pars_list_news(html_ria)
                cosmo_news_list = cosmopolitan_parser.pars_list_news(
                    html_cosmo)

                dot_like_news = admin_news_control.get_news()

                news_list = dot_like_news + ria_news_list + cosmo_news_list + rbk_news_list

                return {"news": news_list, "total": len(news_list)}

            except:
                return {"news": [], "total": []}

        return abort(401)

from flask import request, session
from flask_restful import Resource

from controls import user_control


class Login(Resource):
    def post(self):
        try:
            login = request.form["login"]

            if "admin" in login.lower():
                return {"status": "err"}

            result = user_control.login(request.form)

            if result["status"] == "ok":
                session["user"] = request.form["login"]

            return result

        except:
            return {"status": "err"}

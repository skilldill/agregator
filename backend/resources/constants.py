QUERIES = {
    "rbk": lambda query: f"https://t.rbc.ru/search/?project=rbcnews&query={query}",
    "ria": lambda query: f"https://ria.ru/search/?query={query}",
    "cosmo": lambda query: f"https://www.cosmo.ru/search/?query={query}"
}

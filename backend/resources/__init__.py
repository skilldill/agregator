from controls import UserControl

from .Signin import Signin
from .Login import Login
from .Logout import Logout
from .NewsRecource import NewsRecource
from .LoginAdmin import LoginAdmin
from .LogoutAdmin import LogoutAdmin
from .NewsResourceAdmin import NewsResourceAdmin

from flask import session
from flask_restful import Resource


class LogoutAdmin(Resource):
    def get(self):
        session.pop("admin")
        return {"status": "ok"}

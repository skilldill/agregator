from flask import session, request
from flask_restful import Resource, abort

from controls import admin_news_control


class NewsResourceAdmin(Resource):
    def get(self):
        if session.get("admin"):
            return admin_news_control.get_news()

        return abort(401)

    def post(self):
        if session.get("admin"):
            result = admin_news_control.add_news(request.form)
            return result

        return abort(401)

    def delete(self):
        if session.get("admin"):
            news_id = request.args["news_id"]
            result = admin_news_control.remove_news(news_id)
            return result

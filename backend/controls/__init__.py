import json

from .UserControl import UserControl
from .AdminNewsControl import AdminNewsControl

db_name = ""

with open('config.json', 'r') as f:
    o = json.load(f)
    db_name = o['db']

user_control = UserControl(db_name)
admin_news_control = AdminNewsControl(db_name)

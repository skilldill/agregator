import hashlib

from .BaseControl import BaseControl


class UserControl(BaseControl):
    def get_hash(self, password):
        hash = hashlib.sha1(password.encode())
        result = hash.hexdigest()
        return result

    def signin(self, user_form):
        QUERY_ADD_LOGIN = """
            INSERT INTO users
            (login) VALUES (?)
        """

        QUERY_ADD_PASSWORD = """
            INSERT INTO passwords
            (password, user_id)
            VALUES
            (?, ?)
        """

        QUERY_CHECK_USER = '''
            SELECT id FROM users
            WHERE
            login = (?)
        '''

        result = self.cursor.execute(QUERY_CHECK_USER, (user_form['login'],))
        response = result.fetchone()

        if response is None:
            try:
                cursor = self.cursor.execute(
                    QUERY_ADD_LOGIN, (user_form['login'],))
                user_id = cursor.lastrowid
                password = self.get_hash(user_form['password'])
                self.cursor.execute(QUERY_ADD_PASSWORD, (password, user_id))
                self.conn.commit()

                return {'status': 'ok'}

            except:
                return {'status': "err"}

        return {'status': 'user already exist'}

    def login(self, user_form):
        QUERY_GET_LOGIN = '''
            SELECT id FROM users
            WHERE login = (?)
        '''

        QUERY_GET_PASSWORD = '''
            SELECT id FROM passwords
            WHERE
            user_id = (?)
            AND
            password = (?)
        '''

        result = self.cursor.execute(QUERY_GET_LOGIN, (user_form['login'],))

        response = result.fetchone()

        if response is not None:
            result = result.execute(QUERY_GET_PASSWORD, (
                response[0],
                self.get_hash(user_form['password'])
            ))

            result_check = result.fetchone()

            if result_check is not None:
                return {'status': 'ok'}

            return {'status': 'err'}

        return {'status': 'err'}

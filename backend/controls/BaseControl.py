import sqlite3


class BaseControl:
    def __init__(self, db_name=None):
        self.conn = sqlite3.connect(
            db_name, check_same_thread=False) if db_name else None
        self.cursor = self.conn.cursor()

from .BaseControl import BaseControl


class AdminNewsControl(BaseControl):
    def add_news(self, news_form):
        QUERY_ADD_NEWS = """
            INSERT INTO agregated_news
            (title, content, date)
            VALUES
            (?, ?, ?)
        """
        try:
            self.cursor.execute(
                QUERY_ADD_NEWS, (news_form["title"], news_form["content"], news_form["date"]))
            self.conn.commit()

            return {"status": "ok"}

        except Exception as e:
            return {"status": str(e)}

    def get_news(self):
        QUERY_GET_NEWS = """
            SELECT * FROM agregated_news
        """

        try:
            result = self.cursor.execute(QUERY_GET_NEWS)
            list_news = result.fetchall()

            print(list_news)

            prepared_list_news = []

            for news in list_news:
                news_id = news[0]
                title = news[1]
                content = news[2]
                date = news[3]
                img = news[4]

                prepared_list_news.append({
                    "id": news_id,
                    "title": title,
                    "content": content,
                    "date": date,
                    "img": img
                })

            return prepared_list_news

        except Exception as e:
            return {"status": str(e)}

    def remove_news(self, news_id):
        QUERY_REMOVE = """
            DELETE FROM agregated_news
            WHERE id = (?)
        """

        try:
            self.cursor.execute(QUERY_REMOVE, (news_id,))
            self.conn.commit()
            return {"status": "ok"}

        except:
            return {"status": "err"}

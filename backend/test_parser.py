from bs4 import BeautifulSoup as bs
from requests import request
from pprint import pprint

from parsers import RbkParser, CosmopolitanParser, RiaParser


rbkParser = RbkParser()
cosmopolitanParser = CosmopolitanParser()
riaParser = RiaParser()

query_news = "https://ria.ru/search/?query=Тюмень"

result = request("get", query_news)
html = result.text

# pprint(html)

pprint(riaParser.pars_list_news(html))
